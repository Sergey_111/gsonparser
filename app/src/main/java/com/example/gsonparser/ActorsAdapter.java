package com.example.gsonparser;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ActorsAdapter extends RecyclerView.Adapter<ActorsAdapter.MyViewHolder> {

    private List<Actor> actorList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, gender, aliases;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            aliases = view.findViewById(R.id.aliases);
            gender = view.findViewById(R.id.gender);
        }
    }


    public ActorsAdapter(List<Actor> actorList) {
        this.actorList = actorList;
    }

    void loadNewData(List<Actor> newActors) {
        actorList = newActors;
        notifyDataSetChanged();
    }

    public Actor getActor(int position) {
        return ((actorList != null) && (actorList.size() != 0) ? actorList.get(position) : null);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.actor_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Actor movie = actorList.get(position);
        holder.name.setText(movie.getName());
        holder.aliases.setText(movie.getAliases().get(0));
        holder.gender.setText(movie.getGender());
    }

    @Override
    public int getItemCount() {
        if (actorList != null) {
            return actorList.size();
        } else return 0;
    }


}