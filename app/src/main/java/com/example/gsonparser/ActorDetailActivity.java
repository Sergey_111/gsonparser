package com.example.gsonparser;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class ActorDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actor_detail);
        Intent intent = getIntent();
        Actor actor = (Actor) intent.getSerializableExtra(MainActivity.ACTOR_TRANSFER);
        if (actor != null) {
            TextView name = findViewById(R.id.actor_name);
            name.setText("Name: " + actor.getName());
            TextView gender = findViewById(R.id.actor_gender);
            gender.setText("Gender: " + actor.getGender());
            TextView culture = findViewById(R.id.actor_culture);
            culture.setText("Culture: " + actor.getCulture());
            TextView born = findViewById(R.id.actor_born);
            born.setText("Born: " + actor.getBorn());
            TextView died = findViewById(R.id.actor_died);
            died.setText("Died: " + actor.getDied());
            TextView titles = findViewById(R.id.actor_titles);
            titles.setText("Titles: " + actor.getTitles().toString().replaceAll("^\\[|\\]$", ""));
            TextView aliases = findViewById(R.id.actor_aliases);
            aliases.setText("Aliases: " + actor.getAliases().toString().replaceAll("^\\[|\\]$", ""));
            TextView father = findViewById(R.id.actor_father);
            father.setText("Father: " + actor.getFather());
            TextView mother = findViewById(R.id.actor_mother);
            mother.setText("Mother: " + actor.getMother());
            TextView spouse = findViewById(R.id.actor_spouse);
            spouse.setText("Spouse: " + actor.getSpouse());
            TextView allegiances = findViewById(R.id.actor_allegiances);
            allegiances.setText("Allegiances: " + actor.getAllegiances().toString().replaceAll("^\\[|\\]$", ""));
            TextView books = findViewById(R.id.actor_books);
            books.setText("Books: " + actor.getBooks().toString().replaceAll("^\\[|\\]$", ""));
            TextView povBooks = findViewById(R.id.actor_povBooks);
            povBooks.setText("PovBooks: " + actor.getPovBooks().toString().replaceAll("^\\[|\\]$", ""));
            TextView tvSeries = findViewById(R.id.actor_tvSeries);
            tvSeries.setText("TvSeries: " + actor.getTvSeries().toString().replaceAll("^\\[|\\]$", ""));
            TextView playedBy = findViewById(R.id.actor_playedBy);
            playedBy.setText("PlayedBy: " + actor.getPlayedBy().toString().replaceAll("^\\[|\\]$", ""));
        }
    }

}
