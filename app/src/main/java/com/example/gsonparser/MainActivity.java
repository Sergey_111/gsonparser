package com.example.gsonparser;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RecyclerItemClickListener.OnRecyclerClickListener {

    private List<Actor> actorList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ActorsAdapter actorAdapter;
    HttpURLConnection connection = null;
    BufferedReader reader = null;
    static final String ACTOR_TRANSFER = "ACTOR_TRANSFER";
    private String page = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadActors(page);
        recyclerView = findViewById(R.id.recycler_view);
        actorAdapter = new ActorsAdapter(actorList);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                loadActors(String.valueOf(currentPage));
            }
        });
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(actorAdapter);
    }

    private void loadActors(String page) {
        new ApiConnect().execute("https://www.anapioficeandfire.com/api/characters?page=" + page + "&pageSize=50");
    }

    public String getData(String urlTarget) {
        String line = "";
        try {
            URL url = new URL(urlTarget);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            InputStream stream = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(stream));
            StringBuffer buffer = new StringBuffer();
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
            return buffer.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection.disconnect();
            try {
                if (reader != null) reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void processJson(String json) throws JSONException {

        System.out.println("test112 " + actorList.size());
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Actor>>() {
        }.getType();
        List<Actor> posts = gson.fromJson(json, listType);
        System.out.println("test15 " + posts);
        if (actorList.size() > 0) {
            actorList.addAll(posts);
        } else {
            actorList = posts;
        }
        actorAdapter.loadNewData(actorList);
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(this, ActorDetailActivity.class);
        intent.putExtra(ACTOR_TRANSFER, actorAdapter.getActor(position));
        startActivity(intent);
    }

    class ApiConnect extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            return getData(params[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                processJson(s);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
